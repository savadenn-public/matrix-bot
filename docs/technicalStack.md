# Technical stack

* [Ts.ED](https://tsed.io) for project bootstrap
* [Matrix Javascript SDK](https://github.com/matrix-org/matrix-js-sdk)
  * This is the [Matrix](https://matrix.org) Client-Server r0 SDK for JavaScript.
  * [Olm](https://packages.matrix.org/npm/olm/) library for encryption see [matrix readme](https://github.com/matrix-org/matrix-js-sdk#end-to-end-encryption-support)
