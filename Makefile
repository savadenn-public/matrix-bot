#-----------------------------------------
# Variables
#-----------------------------------------
MKFILE_PATH := $(abspath $(lastword ${MAKEFILE_LIST}))
PROJECT_PATH := $(dir ${MKFILE_PATH})
PROJECT_NAME := $(shell basename ${PROJECT_PATH})
export PROJECT_NAME
PROJECT_URL=${PROJECT_NAME}.docker.localhost
export PROJECT_URL
UID=$(shell id -u)
export UID
GID=$(shell id -g)
export GID

#-----------------------------------------
# Help commands
#-----------------------------------------
.PHONY:
.DEFAULT_GOAL := help

help: ## Prints this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' ${MAKEFILE_LIST} | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

clean: ## Cleans up environnement
	@mkdir -p ./public
	@rm -rf ./public/*
	@docker-compose down --remove-orphans
	@docker-compose pull || true

dev: clean build ## Starts dev stack with TDD
	@docker-compose up --force-recreate

sh: ## Open bash in container loaded with vue-cli
	@docker-compose run --rm server bash

test.coverage: ## Runs unit tests with code coverage
	@docker-compose run --rm server sh -c 'yarn && yarn run test:coverage'

build.node: ## Node build for production
	@docker-compose run --rm server sh -c 'yarn && yarn run build'

build.docker: ## Package dist to docker imahe
	@docker build -t ${PROJECT_NAME} .

build: build.node build.docker ## Build complete image for production

start: ## Run production image
	@docker run --rm -it ${PROJECT_NAME} sh
